#1GAM
======

**#1GAM** is a one game a month game. #1GAM

## Download
* [master](https://bitbucket.org/sreal/#1GAM/get/master.tar.gz)

## Usage
    $ git clone git@bitbucket.org:sreal/#1GAM.git

## Version
* Version -0.0.1

## License
* See [MIT License](license.md)

## Thanks
* See [Thanks and attribution](attribution.md)

## Contact
* Homepage: http://sreal.github.io/jekyll
* e-mail: simon.richard.eames@gmail.com
* Twitter: [@sreal](https://twitter.com/sreal "sreal on twitter")

----
[![Flattr this git repo](http://api.flattr.com/button/flattr-badge-large.png)](https://flattr.com/submit/auto?user_id=sreal&url=https://bitbucket.org/sreal/#1GAM)
